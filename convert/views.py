from django.http import HttpResponse
from django.shortcuts import render, redirect

def home_page(request):
    if request.method == 'POST':
        num = int(request.POST['num'])
        biner = convert(num)
    else:
        num = '-'  
        biner = '-'

    return render(request, 'home.html', {'num':num, 'bin':biner})

def convert(num):
	if (num<0):
		negative = True
		num *= -1
	elif(num == 0):
		return 0
	else:
		negative = False

	biner = ''
	while (num > 0):
		if (num % 2 == 0):
			biner = '0' + biner
		else:
			biner = '1' + biner
		num //= 2
	if (negative):
		return invert(biner)
	else:
		return biner
 
def invert(biner):
	inv = ''
	for char in biner:
		if (char == '0'):
			inv = '1' + inv
		else:
			inv = '0' + inv
	cnt = 0
	ret = ''
	for char in inv:
		cnt += 1
		if (char == '0'):
			ret += '1'
		else:
			ret += '0'
			ret += inv[cnt:]

	return ret[::-1]