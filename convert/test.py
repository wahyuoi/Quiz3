from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string
from convert.views import home_page
import sys


class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)
        
    def test_input_and_show_integer_to_home(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['num'] = '123'

        response = home_page(request)

        self.assertIn("123", response.content.decode())

    def test_convert_positif_integer_to_biner(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['num'] = '123'

        response = home_page(request)

        self.assertIn("Integer : 123", response.content.decode())
        self.assertIn("Biner : 1111011", response.content.decode())

    def test_convert_0_to_biner(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['num'] = '0'

        response = home_page(request)

        self.assertIn("Integer : 0", response.content.decode())
        self.assertIn("Biner : 0", response.content.decode())
    def test_convert_negatif_integer_to_biner(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['num'] = '-123'

        response = home_page(request)

        self.assertIn("Integer : -123", response.content.decode())
        self.assertIn("Biner : 1111111110000101", response.content.decode())