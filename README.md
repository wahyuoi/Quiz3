# Quiz 3 PMPL

#### Soal 1
Pada proyek ini, menggunakan pendekatan TDD. Dari pendekatan ini, maka dibuat unit testing untuk beberapa fitur. Setiap fitur akan dites apakah sesuai dengan test case yang sudah dibuatkan. Ada 3 fase yang bisa dijalanin untuk mendapatkan hasil yang sesuai, yaitu :

- RED, sudah membuat testing, namun belum mengimplementasikan fitur dengan benar. Sehingga akan terjadi 'failure' saat melakukan unit testing
- Green, mulai mengimplementasikan fitur sehingga sesuai dengan testing yang diharapkan. Namun bisa jadi kode yang dibuat belum efisien sekalipun sudah memenuhi testing.
- Refactoring, mengubah kode yang telah dibuat agar bisa lebih efisien dengan tetap menjaga kebenaran dari unit testing yang sudah disediakan.